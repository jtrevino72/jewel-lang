
## Intérprete lenguaje Jewel en Bash

Este es el repositorio de Jewel lang, un lenguaje de programación interpretado que realiza el procesamiento en bash.

Para correr el proyecto ver las siguientes instrucciones.

1. Extraer la carpeta fuente **jewel-lang**.
2. Abrir una terminal.
3. Ejecutar el siguiente comando: **bash jewel.sh *<fuente>*.jwl.**

El archivo **<fuente>.jwl** contiene la programación escrita en Jewel.

El archivo **jewel.sh** actua como el intérprete de código. Se crea un archivo de ejecucion temporal con la extensión ***(.jwx)*** y automaticamente corre el programa escrito. 

Es posible *editar/ejecutar* externamente el archivo temporal ya que se genera una interpretación directa de fuente bash.

---
## Instrucciones para instalar
Ejecutar:

1. "sudo apt-get install git".
2. "sudo git clone https://bitbucket.org/jtrevino72/jewel-lang/ ".
3. "cd jewel-lang".

---
## Instrucciones Soportadas
A continuación la lista de instrucciones soportadas hasta el momento.

1. Instrucción Imprimir (**print** "<mensaje">).
2. Instrucción Condicional simple (**if**(<condicion>){ *bloque*}").
3. Instrucción Ciclo for (**for**(<init>;<condicion>;<incremento>){ *bloque*}").
4. Declaración de variables (var *<identificador>* = *<valor>;*).
5. Soporte de instrucciones anidadas.
