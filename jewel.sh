#!/bin/bash
# .......................................................................
# ........ PROYECTO: Interprete en bash .................................
# ........ ALUMNOS: 1730169, 1730113, 1730122............................
# ........ PROF: Dra. Karla Esmeralda Vazquez ...............................
# ........ UPV - 2018 ...................................................

# Definicion constantes de colores
ROJO='\e[1;31m'
AZUL='\e[1;34m'
VERDE='\e[1;32m'
FIN='\e[0m'
# Bienvenida
echo "++++++ Jewel lang 0.1 ++++++"
echo "Compilando..."
echo "Salida:"
echo ""
# Definicion
contenido=""
identificador=""
l=1 # Contador de linea
err=0 # Bandera de error
bloque=0 # Bandera para indicar la apertura de un bloque
bif=0 # Bandera para la instrucción if

# Crear archivo temp de ejecucion
fex="${1//.jwl/}.jwx" # archivo de ejecucion con el mismo nombre pero con diferente extension
echo >$fex

# Recorrer todo el archivo fuente linea a linea
while IFS='' read -r linea || [[ -n "$linea" ]]; do
    err=0 # reiniciar bandera de error
    #Definicion de las expresiones regulares para identificar instrucciones
	printcmd=`echo $linea | grep -o -P '(?<=print).*(?=;)'`
	forinst=`echo $linea | grep -o -P '(?<=for).*(?={)'`
	ifinst=`echo $linea | grep -o -P '(?<=if).*(?={)'`
	whileinst=`echo $linea | grep -o -P '(?<=while).*(?={)'`
	defvar=`echo $linea | grep -o -P '(?<=var ).*(?=;)'`
	inscoment=`echo $linea | grep -o -P '(?<=--).*(?=--)'`
	readinst=`echo $linea | grep -o -P '(?<=input).*(?=;)'`

	# EVALUACION DEL CIERRE DE UN BLOQUE 
	# Si es el caracter para cerrar bloque (remover los espacios con esa expresion regular)
	if [[ "${linea//[[:blank:]]/}" == "}" ]]; then
		# Cerrar bloque de instruccion
		if [[ $bloque > 0 ]]; then
			# Cerrar bloque si la instruccion es un if
			if [[ $bif == 1 ]]; then
				echo "fi" >> $fex
				bif=0 #reiniciar bandera
			else
				#echo "$linea -> $l"
				echo "done" >> $fex	
			fi

			bloque=$(($bloque-1)) # (reiniciar) desactivar la apertura del bloque
		else
			echo -e "$ROJO Error de sintaxis!:$FIN $AZUL cierre de bloque $FIN- linea $VERDE$l$FIN"
		fi
	fi
	
	# -- COMANDO print --
	# USO: print "<texto>";
	# Si la instruccion es un print
	if [[ !(-z "$printcmd") ]]; then
		# Obtener el texto entre las comillas
		contenido=`echo $linea | grep -o -P '(?<=").*(?=")'`
		# Si se encuentra la sintaxis imprimir el contenido
		if [[ !(-z "$contenido") ]]; then
			#echo $contenido
    		echo "echo \"$contenido\";" >> $fex
    	else
			echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN print - linea $VERDE$l$FIN"
			#echo "Error de sintaxis!: Comando: print - linea $l"
			exit
		fi
	fi

	# -- INSTRUCCION if --
    # USO: if(<cond>){
    # Si la instruccion es un if
    if [[ !(-z "$ifinst") ]]; then
		# Obtener el texto entre los parentesis
		contenido=`echo $linea | grep -o -P '(?<=\().*(?=\))'`
		#echo $contenido
		if [[ !(-z "$contenido") ]]; then
			# Falta evaluar si cada definicion es una expresion valida
			bloque=$(($bloque+1)) #Activar instruccion que requiere 
			bif=1 #bandera de instruccion if
			echo "if [[ $contenido ]]; then" >> $fex
		else
			err=1; #Marcar error
		fi
		# Revisar si hubo error de sintaxis
		if [[ $err == 1 ]]; then
			echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN for - linea $VERDE$l$FIN"
		fi
    fi

    # -- INSTRUCCION for --
    # USO: for(<init>;<cond>;<incr>){
    # Si la instruccion es un for
    if [[ !(-z "$forinst") ]]; then
		# Obtener el texto entre los parentesis
		contenido=`echo $linea | grep -o -P '(?<=\().*(?=\))'`
		#echo $contenido
		if [[ !(-z "$contenido") ]]; then
			numch=`echo "${contenido}" | awk -F";" '{print NF-1}'`
			# Si tiene exactamente dos (;)
			if [[ $numch == 2 ]]; then
				# Falta evaluar si cada definicion es una expresion valida
				bloque=$(($bloque+1)) #Activar instruccion que requiere bloque
				echo "for (( $contenido )); do" >> $fex
			else
				err=1; #Marcar error
			fi
		else
			err=1; #Marcar error
		fi
		# Revisar si hubo error de sintaxis
		if [[ $err == 1 ]]; then
			echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN for - linea $VERDE$l$FIN"
		fi
	fi

	# -- INSTRUCCION while --
    # USO: while(<cond>){
    # Si la instruccions es un while
    if [[ !(-z "$whileinst") ]]; then
		# Obtener el texto entre los parentesis
		contenido=`echo $linea | grep -o -P '(?<=\().*(?=\))'`
		#echo $contenido
		if [[ !(-z "$contenido") ]]; then
			# Falta evaluar si cada definicion es una expresion valida
			bloque=$(($bloque+1)) #Activar instruccion que requiere bloque
			echo "while [[ $contenido ]]; do" >> $fex
		else
			err=1; #Marcar error
		fi
		# Revisar si hubo error de sintaxis
		if [[ $err == 1 ]]; then
			echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN while - linea $VERDE$l$FIN"
		fi
	fi

	# -- INSTRUCCION definicion de variable --
    # USO: var <identificador> = <valor>;
    # Si la instruccions es una definicion de variable
    if [[ !(-z "$defvar") ]]; then
    	# Obtener el texto entre el espacio y el igual
		contenido=`echo $linea | grep -o -P '(?<=var ).*(?=;)'`
		identificador=$contenido
		if [[ !(-z "$contenido") ]]; then
			contenido=`echo $linea | grep -o -P '(?<=\=).*(?=;)'`
			# Si tiene el valor entre el igual y el punto y coma (;)
			if [[ !(-z "$contenido") ]]; then
				# Soportar espacios en el igual 
				identificador="${identificador//[[:blank:]]/}";
				# Si es un operador matematico
				if [[ $identificador = *"*"* ]] || [[ $identificador = *"/"* ]] || [[ $identificador = *"+"* ]] || [[ $identificador = *"-"* ]]; then
					echo "(( $identificador ))" >> $fex
				else
					# -- INSTRUCCION comando read --
				    # USO: input ();
				    # Si la instruccions es un comando read
				    if [[ !(-z "$readinst") ]]; then
				    	# Obtener el texto entre el espacio y el igual
				    	id=`echo $linea | grep -o -P '(?<=var ).*(?=\=)'`
						nump1=`echo "${contenido}" | awk -F"(" '{print NF-1}'`
						nump2=`echo "${contenido}" | awk -F")" '{print NF-1}'`
						# Soportar espacios en el igual 
						id="${id//[[:blank:]]/}";
						
						# Si tiene la sintaxis correcta
						if [[ $nump1 == 1 ]] && [[ $nump2 == 1 ]]; then
							echo "read $id" >> $fex
						else
							err=1
						fi

						# Revisar si hubo error de sintaxis
						if [[ $err == 1 ]]; then
							echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN input - linea $VERDE$l$FIN"
						fi
					else
						echo "$identificador" >> $fex
				    fi
				fi
			else
				err=1	
			fi	
		else
			err=1	
		fi

		# Revisar si hubo error de sintaxis
		if [[ $err == 1 ]]; then
			echo -e "$ROJO Error de sintaxis!:$FIN$AZUL Comando:$FIN definicion de variable - linea $VERDE$l$FIN"
		fi
    fi

    # -- INSTRUCCION comentario --
    # USO: --
    # Si la instruccions es un comentario
    if [[ !(-z "$inscoment") ]]; then
    	echo "# $inscoment" >> $fex
    fi


	# Incrementar contador de linea
	l=$((l+1));
done < "$1"

#Ejecutar archivo temporal de ejecucion
bash $fex